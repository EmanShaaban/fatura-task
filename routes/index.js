const AsyncRouter = require('macropress-router')

const router = new AsyncRouter()

router.use('/', require('./health'))
router.use('/products', require('./product'))
router.use('/categories', require('./category'))

module.exports = router
