const AsyncRouter = require('macropress-router')

const router = new AsyncRouter()

const categoryController = require('../controllers/category')

router.post('/', categoryController.createCategory)
router.get('/', categoryController.listCategories)

module.exports = router
