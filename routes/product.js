const AsyncRouter = require('macropress-router')

const router = new AsyncRouter()

const productController = require('../controllers/product')
// const { toggleFeaturedValidator } = require('../validators/product')
const { paginatorMiddleware } = require('../middlewares/paginator')

router.get('/', paginatorMiddleware, productController.listByCategory)
router.post('/', productController.createProduct)
router.patch('/:id', /* toggleFeaturedValidator, */productController.toggleFeatured)

module.exports = router
