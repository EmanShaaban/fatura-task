const chai = require('chai')
const { app } = require('../app')
chai.use(require('chai-http'))

const expect = chai.expect
const request = chai.request(app).keepOpen()

module.exports = {
  expect,
  request
}
