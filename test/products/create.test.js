const { expect, request } = require('../utils')

describe('Create product', async () => {
  it('should create a product successfully', async () => {
    const res = await request.post('/products')

    console.log(res.body)
    expect(res).to.have.status(200)
  })
})
