const { app } = require('./app')
const logger = require('./config/logger')

const server = app.listen(app.get('port'), () => {
  logger.info('Server Started...')
})

module.exports = { server }
