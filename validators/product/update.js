const Joi = require('@hapi/joi')
const Boom = require('boom')

const toggleFeaturedValidator = Joi.object({
  params: {
    id: Joi.number().required()

  },
  body: Joi.object()
    .min(1)
    .keys({
      featured: Joi.number().valid((0, 1)).required()
        .error(errors =>
          Boom.badRequest(errors[0], {
            field: 'featured'
          })
        )
    })
})

module.exports = {
  toggleFeaturedValidator
}
