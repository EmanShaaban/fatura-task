const models = require('../models')

const createCategory = async (req, res) => {
  const {
    name,
    parent_id: parentId
  } = req.body

  const category = await models.Category.create({
    name,
    parentId: parentId
  })

  return category
}

const listCategories = async () => {
  const categories = await models.Category.findAll({ limit: 10 })
  return categories
}

module.exports = {
  createCategory,
  listCategories
}
