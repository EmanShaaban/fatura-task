const _ = require('lodash')
const Boom = require('boom')
const Sequelize = require('sequelize')

const models = require('../models')

const createProduct = async (req, res) => {
  const {
    name,
    image_url,
    category_id: categoryId,
    featured,
    provider
  } = req.body

  try {
    const product = await models.Product.create({
      name,
      image_url,
      categoryId,
      featured
    })

    // Create provider if provider object exists
    let providerId
    if (!_.isEmpty(provider)) {
      const providerObj = await models.Provider.create({
        name: provider.name
      })
      providerId = providerObj.id
    }

    // Create product_provider entry for product
    let productProviders
    if (!_.isNil(providerId)) {
      productProviders = await models.ProductProvider.create({
        productId: product.id,
        providerId: providerId,
        price: provider.price,
        available: provider.available
      })
    }
    return {
      product,
      product_providers: productProviders
    }
  } catch (err) {
    if (err instanceof Sequelize.ValidationError) {
     // throw Boom.boomify(err, { statusCode: 400 })
      throw Boom.badRequest('Error creating product', err)
    }
    throw err
    
  }
}

const listByCategory = async (req, res) => {
  const { limit, from } = req.parsed.paginator
  const categoryId = parseInt(req.query.category)
  try {
    const products =
        await models.sequelize.query(
            `SELECT  *
             FROM Products         
             LEFT JOIN ProductProviders ON Products.id = ProductProviders.productId
             WHERE categoryId =${categoryId} AND
             ProductProviders.price = (SELECT MIN(price) FROM ProductProviders)
             LIMIT ${limit} OFFSET ${from};
            `)

    return products
  } catch (err) {
    if (err instanceof Sequelize.ValidationError) {
      throw Boom.boomify(err, { statusCode: 400 })
    }
    throw err
  }
}

const toggleFeatured = async (req, res) => {
  const isFeatured = parseInt(req.body.is_featured)
  const productId = req.params.id

  const product = await models.Product.update(
    { featured: isFeatured },
    {
      where: { id: productId }
    }
  )

  return product
}
module.exports = {
  createProduct,
  listByCategory,
  toggleFeatured
}
