const express = require('express')
const compression = require('compression')
const bodyParser = require('body-parser')

const router = require('./routes')
const httpLogger = require('./middlewares/http-logger')

const app = express()

app.set('port', process.env.PORT || 5000)

// Middlewares
app.use(compression())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(httpLogger)
app.use(router)

module.exports = { app }
