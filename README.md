# Fatura task

## Start
``` bash
docker-compose up
```

## Test
> These should be the tests which I didn't have time to complete!
``` bash
$ set -a; source test.env; set +a
$ docker start mysql-container
$ sequelize db:migrate
$ yarn test
```

## App
http://localhost:5000/health

## Documentation
> And The documentauon which I didn't have time to do it either
http://localhost:5000/docs/


# Important NOTE
- Table IDs should be UUIDs
- Price should be stored as Integer number representing `cents` 

### Create product request example:
> Without provider
```
{
	"category_id":5,
	"name": "product name",
}
```

> With provider
```
{
	"category_id":5,
	"name": "product name",
	"provider": {
		"name":"prvider name",
		"price": "30",
	    "available": 1
	}
}
```