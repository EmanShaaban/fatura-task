'use strict'
const {
  Model
} = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class ProductProvider extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate (models) {
      // define association here
    }
  };
  ProductProvider.init({
    price: {
      type: DataTypes.DOUBLE,
      allowNull: false
    },
    available: DataTypes.TINYINT(1),
    productId: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    providerId: {
      type: DataTypes.INTEGER
    }
  }, {
    sequelize,
    modelName: 'ProductProvider'
  })
  return ProductProvider
}
