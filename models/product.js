'use strict'
const {
  Model
} = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class Product extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate (models) {
      // define association here
      Product.hasMany(models.ProductProvider)
    }
  };
  Product.init({
    name: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    image_url: DataTypes.STRING(255),
    featured: DataTypes.TINYINT(1),
    categoryId: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, {
    sequelize,
    modelName: 'Product'
  })
  return Product
}
