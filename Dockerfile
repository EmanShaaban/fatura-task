FROM node:14.15.3-alpine

WORKDIR /usr/src/app

COPY package.json /usr/src/app/
COPY yarn.lock /usr/src/app/
RUN yarn
COPY . /usr/src/app
EXPOSE 3000


CMD ["yarn", "start"]
