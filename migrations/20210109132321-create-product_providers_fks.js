'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn(
      'productproviders',
      'productId',
      {
        type: Sequelize.INTEGER,
        references: {
          model: 'products',
          key: 'id'
        },
        allowNull: false
      }
    )

    await queryInterface.addColumn(
      'productproviders',
      'providerId',
      {
        type: Sequelize.INTEGER,
        references: {
          model: 'providers',
          key: 'id'
        },
        allowNull: true
      }
    )
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('productproviders', 'productId')
    await queryInterface.removeColumn('productproviders', 'providerId')
  }
}
